// console.log("Hello world");

let number = Number(prompt("Input number:"));
console.log("The number you provided is " + number);
for(number; number > 0; number--){
	if(number <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	else if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	else if(number % 5 === 0){
		console.log(number);
	}
}

let word = "supercalifragilisticexpialidocious";
console.log(word);
let consonants = "";
for(let i = 0; i < word.length; i++){
	if(word[i] === "a" ||
		word[i] === "e" ||
		word[i] === "i" ||
		word[i] === "o" ||
		word[i] === "u"){
		continue;
	}
	else{
		consonants += word[i];
	}
}

console.log(consonants);